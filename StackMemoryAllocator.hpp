//-----------------------------------------------------------------------------
// Copyright (c) 2015 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef StackMemoryAllocator_hpp
#define StackMemoryAllocator_hpp

#include <cassert>
#include <stdio.h>
#include <cstddef>
#include <cstdint>

namespace MemoryManagement 
{

	class StackMemoryAllocator 
	{
	public:
		//also known as a marker, thse are pointers used to navigate within the stack
		typedef uintptr_t StackPtr;

		//CTOR
		explicit StackMemoryAllocator(const size_t stackSize) : m_StackStart(0), m_StackEnd(0), m_StackTop(0) 
		{
			initializeStack(stackSize);
		}

		//prevent construction from copies
		StackMemoryAllocator(const StackMemoryAllocator&) = delete;

		//prevent assignment
		StackMemoryAllocator& operator=(const StackMemoryAllocator&) = delete;

		//DTOR
		~StackMemoryAllocator() 
		{
			clearStackMemory();
		}

		//allocate memory in this stack and return a pointer to it
		//will return an aligned memory address for the size specified
		//will also allocate in total  (size + aligment) bytes to account for the alignment execution
		//alignment should be a power of two!
		void* allocate(const size_t size, const size_t alignment);

		//frees the memory to a previous stack top marker
		//the pointer is the one that the user got from allocation, so it is an aligned memory address
		void deallocate(void* ptr);

		//reset the entire stack by setting the stack top marker to the initial marker
		void reset();

		//gets the stack size in bytes
		inline size_t getSize() const 
		{
			return m_StackEnd - m_StackStart;
		}

	private:
		//pre allocates all memory that can be used by this allocator
		void initializeStack(const size_t stackSize);

		//clears all memory used by this stack allocator
		void clearStackMemory();

		//allocate memory in this stack and return a pointer to it
		//no alignment is executed
		void* allocateUnaligned(const size_t size);

		//rollback the stack top to a previous stack top marker from an unaligned memory address
		void freeToMarker(StackPtr unalignedMarker);

		//the stack head pointer
		StackPtr m_StackStart;

		//the stack end pointer
		StackPtr m_StackEnd;

		//the current stack top marker
		StackPtr m_StackTop;
	};


}

#endif
