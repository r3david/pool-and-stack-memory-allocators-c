//-----------------------------------------------------------------------------
// Copyright (c) 2015 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef PoolMemoryAllocator_hpp
#define PoolMemoryAllocator_hpp

#include <stdio.h>
#include <cstddef>
#include <cstdint>

namespace MemoryManagement 
{

	//linked list of objects
	struct PoolMemoryBlock 
	{
		PoolMemoryBlock* next;
	};

	class PoolMemoryAllocator 
	{
	public:
		//CTOR
		explicit PoolMemoryAllocator(const size_t blockSize, const size_t alignment) : m_BlockAlignment(alignment), m_FreeBlocksPoolHead(nullptr) 
		{
			//size of a memory block should be at least bigger than the size of a pointer, since blocks contain themselves pointers to the next block
			size_t poolMemoryBlockSize = sizeof(PoolMemoryBlock*);
			m_BlockSize = (blockSize > poolMemoryBlockSize) ? blockSize : poolMemoryBlockSize;

			//initialize the pool to contain some preallocated blocks
			expandPoolSize();
		}

		//prevent construction from copies
		PoolMemoryAllocator(const PoolMemoryAllocator&) = delete;

		//prevent assignment
		PoolMemoryAllocator& operator=(const PoolMemoryAllocator&) = delete;

		//DTOR
		~PoolMemoryAllocator() 
		{
			clearPoolMemory();
		}

		//will get the first free block in the pool
		//if the pool is without any free blocks left, this will expand the pool and return the new free one
		//the blocks are all aligned in memory according to the CTOR of this pool
		void* allocate(const size_t size);

		//marks the block passed by as a free block,
		//so it can be reused next time we request for a new block allocation
		void free(void* deleted);

	private:
		//expands the pool size, will use "new" to preallocate memory for a set blocks that can be reused for this pool
		void expandPoolSize();

		//clears all unused memory blocks in this pool allocator
		//this will actually releases the memory
		void clearPoolMemory();

		//allocate an object for this pool and returns a pointer to it
		//returns an aligned memory address
		//this pool allocates all blocks of the same size as specified in the CTOR
		//aligment is constant as well specified in the CTOR of the pool
		PoolMemoryBlock* allocateBlock();

		//frees the memory of the pointer specified
		//ptr is an aligned memory address that the user gets from the allocateBlock method
		void deallocateBlock(PoolMemoryBlock* ptr);

		//the head of the linked list of free memory blocks
		PoolMemoryBlock* m_FreeBlocksPoolHead;

		//the size of the type of blocks that this pool allocates
		size_t m_BlockSize;

		//the alignment requirement for the blocks
		size_t m_BlockAlignment;
	};

}

#endif
