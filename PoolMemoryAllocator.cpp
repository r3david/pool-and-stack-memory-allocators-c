//-----------------------------------------------------------------------------
// Copyright (c) 2015 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "PoolMemoryAllocator.hpp"

namespace MemoryManagement 
{

	void PoolMemoryAllocator::expandPoolSize() 
	{
		const unsigned int POOL_SIZE = 32; //we pre allocate 32 blocks each time we run out of memory when requesting blocks

		//set the head of the free blocks pool (linked list)
		m_FreeBlocksPoolHead = allocateBlock();

		//allocate memory blocks ahead of time, they remain free until usage requested
		PoolMemoryBlock* current = m_FreeBlocksPoolHead;
		for (unsigned int i = 0; i<POOL_SIZE; ++i)
		{
			current->next = allocateBlock();
			current = current->next;
		}
		current->next = nullptr;
	}

	void PoolMemoryAllocator::clearPoolMemory() 
	{
		PoolMemoryBlock* current = m_FreeBlocksPoolHead;
		for (; current != nullptr; current = m_FreeBlocksPoolHead) 
		{
			m_FreeBlocksPoolHead = m_FreeBlocksPoolHead->next;
			deallocateBlock(current);
		}
	}

	PoolMemoryBlock* PoolMemoryAllocator::allocateBlock() 
	{
		//the total size of a block is actually bigger, to allow alignment
		size_t expandedBlockSize = m_BlockSize + m_BlockAlignment;

		//allocate memory getting an unaligned memory address
		uintptr_t unalignedAddress = reinterpret_cast<uintptr_t>(new char[expandedBlockSize]);

		//get the offset of the unaligned address to the next "greater" aligned address for our target size
		ptrdiff_t addressOffset = m_BlockAlignment - (unalignedAddress & (m_BlockAlignment - 1));

		//store the address offset to the unaligned memory address in the byte just before the aligned address
		uintptr_t alignedAddress = unalignedAddress + addressOffset;
		uint8_t* alignedAddPtr = reinterpret_cast<uint8_t*>(alignedAddress);
		alignedAddPtr[-1] = static_cast<uint8_t>(addressOffset);

		//return the aligned address as a block pointer
		return reinterpret_cast<PoolMemoryBlock*>(alignedAddress);
	}

	void PoolMemoryAllocator::deallocateBlock(PoolMemoryBlock* ptr) 
	{
		uintptr_t alignedAddress = reinterpret_cast<uintptr_t>(ptr);

		//get the offset to the actual unaligned memory address that was used for computing the aligned address of ptr
		const uint8_t* alignedAddPtr = reinterpret_cast<const uint8_t*>(alignedAddress);
		ptrdiff_t addressOffset = static_cast<ptrdiff_t>(alignedAddPtr[-1]);

		//now we get the unaligned memory address marker
		uintptr_t unalignedAddress = alignedAddress - addressOffset;

		//delete the memory we allocated for this block 
		delete[] reinterpret_cast<char*>(unalignedAddress);
	}

	void* PoolMemoryAllocator::allocate(const size_t size) 
	{
		//check if we ran out of memory on the pool, then expand
		if (m_FreeBlocksPoolHead == nullptr) 
			expandPoolSize();

		//get the pointer to the first free block in the pool
		PoolMemoryBlock* currentFree = m_FreeBlocksPoolHead;

		//update the head of the free blocks pool
		//when executing at the end of the pool, this will set m_FreeBlocksPoolHead a nullptr
		//so next allocation will expand the pool size
		m_FreeBlocksPoolHead = m_FreeBlocksPoolHead->next;

		return static_cast<void*>(currentFree);
	}

	void PoolMemoryAllocator::free(void* deleted) 
	{
		//simply put that memory block we want to delete at the beginning of the free blocks pool
		PoolMemoryBlock* block = static_cast<PoolMemoryBlock*>(deleted);
		block->next = m_FreeBlocksPoolHead;
		m_FreeBlocksPoolHead = block;
	}

}

