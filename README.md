Copyright (c) 2015 Ricardo David CM - http://ricardo-david.com  

### CONTENT 

This repository contains an implementation of a custom Pool and Stack memory allocators with memory alignments in C++. 

Reference : https://www.amazon.com/Engine-Architecture-Second-Jason-Gregory/dp/1466560010


-------------------------------------------