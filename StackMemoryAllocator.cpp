//-----------------------------------------------------------------------------
// Copyright (c) 2015 Ricardo David CM (http://ricardo-david.com),
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "StackMemoryAllocator.hpp"

namespace MemoryManagement 
{

	void StackMemoryAllocator::initializeStack(const size_t stackSize) 
	{
		//preallocate all stack memory
		m_StackStart = reinterpret_cast<StackPtr>(new char[stackSize]);

		//initialize stack markers
		m_StackEnd = m_StackStart + stackSize;
		m_StackTop = m_StackStart;
	}

	void StackMemoryAllocator::clearStackMemory() 
	{
		delete[] reinterpret_cast<char*>(m_StackStart);
		m_StackStart = 0;
		m_StackEnd = 0;
		m_StackTop = 0;
	}

	void* StackMemoryAllocator::allocateUnaligned(const size_t size) 
	{
		//return the current stack top, and update it accordignly
		//no checks for out of memory here, since this method is private and the callee checks for out of memory
		void* top = reinterpret_cast<void*>(m_StackTop);
		m_StackTop = m_StackTop + size;
		return top;
	}

	void StackMemoryAllocator::freeToMarker(StackPtr unalignedMarker) 
	{
		//simply rollback the stack top to the marker specified
		m_StackTop = unalignedMarker;
	}

	void* StackMemoryAllocator::allocate(const size_t size, const size_t alignment) 
	{
		//check the aligment is passed correctly as power of 2
		assert((alignment & (alignment - 1)) == 0);

		//get the total size to allocate
		size_t totalAllocationSize = size + alignment;

		//check for out of memory
		if (m_StackTop + totalAllocationSize > m_StackEnd) 
			return nullptr;

		//allocate memory without alignment        
		StackPtr unalignedAddress = reinterpret_cast<StackPtr>(allocateUnaligned(totalAllocationSize));

		//get the offset of the unaligned address to the next "greater" aligned address for our target size
		ptrdiff_t addressOffset = alignment - (unalignedAddress & (alignment - 1));

		//store the address offset to the unaligned memory address in the byte just before the aligned address
		StackPtr alignedAddress = unalignedAddress + addressOffset;
		uint8_t* alignedAddPtr = reinterpret_cast<uint8_t*>(alignedAddress);
		alignedAddPtr[-1] = static_cast<uint8_t>(addressOffset);

		//return the aligned address
		//the memory between the previous stack top address, to this aligned address is not used,
		//except for the single byte before this aligned address used for storing the offset, which in turn
		//is used for memory deallocation
		return reinterpret_cast<void*>(alignedAddress);
	}

	void StackMemoryAllocator::deallocate(void *ptr) 
	{
		StackPtr alignedAddress = reinterpret_cast<StackPtr>(ptr);

		//get the offset to the actual unaligned memory address that was used for computing the aligned address of ptr
		const uint8_t* alignedAddPtr = reinterpret_cast<const uint8_t*>(alignedAddress);
		ptrdiff_t addressOffset = static_cast<ptrdiff_t>(alignedAddPtr[-1]);

		//now we get the unaligned memory address marker
		StackPtr unalignedAddress = alignedAddress - addressOffset;

		//and rollback to that memory address
		freeToMarker(unalignedAddress);
	}

	void StackMemoryAllocator::reset() 
	{
		//rollback the stack top to the start of the stack
		freeToMarker(m_StackStart);
	}

}